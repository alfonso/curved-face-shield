# Curved Shield

## Folded Face Shield

(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and displayed for any purpose, but must acknowledge the MIT Curved Shield project. Copyright is retained and must be preserved. The work is provided as-is; no warranty is provided, and users accept all liability.

![Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(1).jpeg](Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(1).jpeg)

## Design Evolution

A need  for a fast-to-manufacture, cheap, and fast to assembly face mask is needed under this COVID crisis. Following the same strategy as the isolation box project, a PETG design based on the possibility to be milled as Zach FabShield approach was done this week. The design still iterating but a reasonable size has been reached as well as a nice lateral, top and lower protection. 

![Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/Captura_de_pantalla_2020-05-11_a_las_19.27.05.png](Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/Captura_de_pantalla_2020-05-11_a_las_19.27.05.png)

### V0 and V1

An overall rounded shape, influenced by the Decathlon design was attempted. Also, we decided to not use the direct hinge line and take advantage of the curved formed when bending  the PETG. So discretizing along multiple lines, we are able to generate a faceish shape when folded, providing an interesting curvature. 

![Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_19.07.07.jpeg](Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_19.07.07.jpeg)

![Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_19.07.08.jpeg](Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_19.07.08.jpeg)

### V2. Shaping the face form

It was needed to make a wider design of a face. Also, to provide a comfortable face disposition due to the plastic were touching the face. As it can be seen in the V2 scheme upwards, the chin fold were so prominent and uncomfortable to look down with the mask on.  

### V3. Making it more structurally stable, designing better snap fits and increasing comfort.

After those iterations, the design that came was the following. 

![Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(5).jpeg](Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(5).jpeg)

The time of cutting at the Zund, using a v21 blade and 300mm/s of linear velocity was 38 seconds. The side shape has been also re-designed to, when folded, the edges shape is functional and doesn't interface with the rest of the body. 

![Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(3).jpeg](Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(3).jpeg)

Also, when looked in frontal view, it looks well. 

![Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(4).jpeg](Curved%20Shield%20a5ceaa12053b49cc973e136059016f14/WhatsApp_Image_2020-05-11_at_17.32.00_(4).jpeg)

Some improvement for the next days are: 

- [ ]  Remove the necessity of the elastic band
- [ ]  Fix the lateral discontinuity
- [ ]  Cover (maybe?) a little more the under area
- [ ]  Upload DXFs